variable Azones { description = "zones disponibles" }
variable Azone { description = "zona única a assignada" }
variable Azone2 { description = "zona única b assignada" }

////////////////////////////////////////////////////////////

resource "aws_lb" "loadbalancer" {
  name               = "terraform-alb"
  internal = false // el ALB està exposat fora
  load_balancer_type = "application" // default
  subnets =  [aws_subnet.public[0].id,aws_subnet.public[1].id]
  security_groups = [aws_security_group.permetre_http_s.id,]
  enable_deletion_protection = false
  tags = {Name = "terraform-alb"}
}

resource "aws_lb_listener" "web_http" { 
  load_balancer_arn = aws_lb.loadbalancer.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.GrupInstancies.arn   // No cal si són varis grups
  }
}

resource "aws_alb_target_group_attachment" "tgattachment" {
  count            = length(aws_instance.instance.*.id) == 2 ? 2 : 0 // Aixecarem 2 instàncies
  target_group_arn = aws_lb_target_group.GrupInstancies.arn
  target_id        = element(aws_instance.instance.*.id, count.index)
}

resource "aws_lb_target_group" "GrupInstancies" {
  name     = "targetgrouplb1"
  port     = 80
  protocol = "HTTP"
  //target_type = "ip"
  vpc_id   = aws_vpc.main_vpc.id


  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    interval            = 30
  }

}
  // FINS AQUI BALANCEJADOR

/////////////////////////////////////////////////////////////////////////////////

variable ssh_pub { 
  description = "ruta on estan la clau pública en local i que es propagarà per a cada instancia" 
}

resource "aws_key_pair" "key_pair_server_web" {   // on és la clau pública de la maquina ssh
  key_name   = "server_web_key"
  public_key = file(var.ssh_pub)  
}

resource "aws_instance" "instance" {
  count                = length(aws_subnet.public.*.id)
  ami = data.aws_ami.server_web.id 
  instance_type = "t3.micro"
  subnet_id            = element(aws_subnet.public.*.id, count.index)
  security_groups      = [aws_security_group.permetre_http_s.id, ]
  key_name =  "server_web_key"
  user_data = file("userdata2.tpl")  // executar script
  monitoring = true //new

  tags = {
    "Name"        = "Instance-${count.index}"
    "Environment" = "Test"
    "CreatedBy"   = "Terraform"
  }

  timeouts {
    create = "10m"
  }
}
