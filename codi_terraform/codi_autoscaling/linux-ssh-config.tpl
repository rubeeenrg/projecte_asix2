
{%* Script per accedir via visualstudio ssh a la AMI *%}

cat << EOF >> ~/.ssh/config

Host ${hostname}
  HostName ${hostname}
  User ${user}
  IdentityFile ${identityfile}
EOF