///////// IGW
resource "aws_internet_gateway" "main_internet_gateway" { # creem una porta de sortida a internet(GW)
  vpc_id = aws_vpc.main_vpc.id  
  tags = {Name = "principal-igw"}
}


//NAT DE TOTA LA VIDA, SORTIR A INTERNET O ANAR A UNA ALTRE XARXA

resource "aws_nat_gateway" "nat1" { 
  count         = "1"
  allocation_id = aws_eip.loadbalancer[count.index].id                      //AIXO ES UNA VAR??
  subnet_id = aws_subnet.public_subnet.id
  depends_on    = [aws_internet_gateway.main_internet_gateway]
  tags = {
   Name        = "nat_illa"
  }
}

resource "aws_nat_gateway" "nat2" { 
  count         = "1"
  allocation_id = aws_eip.loadbalancer2[count.index].id                          //AIXO ES UNA VAR??
  subnet_id = aws_subnet.public_subnet_2.id
  depends_on    = [aws_internet_gateway.main_internet_gateway]
  tags = {
   Name        = "nat_illa2"
  }
}

#  VPC

resource "aws_vpc" "main_vpc" {
  cidr_block           = "10.0.0.0/16"
  //enable_dns_hostnames = true # Per defecte ja ve a "true", estem activant la resolució DNS dins la VPC.
  //enable_dns_support   = true
  tags = {Name = "principal"}
}

///////////////////////////////////////////////////////////////////////////////////////////////////


///////////// INTERFACE  ESTO NO LO TIENE !!!! 10.0.0.53 assigna
/*

resource "aws_network_interface" "lb-nic" {
 subnet_id = aws_subnet.public_subnet.id
 private_ips = ["10.0.0.50"]  // ok
 security_groups = [aws_security_group.permetre_http_s.id]
//attachment {
  //instance     = aws_instance.test.id
    //device_index = 1
//}
}
*/
////////////// Public Elastic IP 
resource "aws_eip" "loadbalancer" {  //OJU AMB EL NOM, REPE !!
 //network_interface = aws_network_interface.lb-nic.id  // AJSD
  //allocation_id = aws_nat_gateway.nat.id  //     CICLTE??
 //associate_with_private_ip="10.0.0.50" // OPCIONAL, POTSER CAL PER A LA ESTATIC??
  count = "1"
  vpc      = true
  depends_on                = [aws_internet_gateway.main_internet_gateway]
}

resource "aws_eip" "loadbalancer2" {  //OJU AMB EL NOM, REPE !!
 //network_interface = aws_network_interface.lb-nic.id  // AJSD
  //allocation_id = aws_nat_gateway.nat.id  //     CICLTE??
 //associate_with_private_ip="10.0.0.50" // OPCIONAL, POTSER CAL PER A LA ESTATIC??
  count = "1"
  vpc      = true
  depends_on                = [aws_internet_gateway.main_internet_gateway]
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////7

# SUBNET PUBLICA 1

resource "aws_subnet" "public_subnet" {
  vpc_id                  = aws_vpc.main_vpc.id # referenciem el valor de la "id" de la nostra subnet (main_vpc)
  cidr_block              = "10.0.10.0/24"  //cambiat 0 per 10
  map_public_ip_on_launch = true         # Quan inicï la "public subnet", se li assginarà una IP pública existent.
  availability_zone       = var.Azone # Escollim "a" per si necessitem utilitzar més "Availiability Zones" en un futur.
  tags = {Name = "principal-public"}   // DE FET AQUESTA ES LA NETWORK LOAD BALANCER QUE MENCIONEN
}

resource "aws_route_table" "main_public_rt_1" { # route table per a la subxarxa pública
  count="1"
  vpc_id = aws_vpc.main_vpc.id
  tags = {Name = "principal_public_rt_1"}
}

resource "aws_route" "default_route_pu1" {   // default_route, podem sortir on volguem
  count = "1"
  route_table_id = aws_route_table.main_public_rt_1[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main_internet_gateway.id
}

resource "aws_route_table_association" "main_public_assoc" { # vinculem la subxarxa publica a la taula d'enrutament
  count="1"
  subnet_id      = aws_subnet.public_subnet.id
  route_table_id = aws_route_table.main_public_rt_1[count.index].id
}

# SUBNET PUBLICA 2

resource "aws_subnet" "public_subnet_2" {
  vpc_id                  = aws_vpc.main_vpc.id # referenciem el valor de la "id" de la nostra subnet (main_vpc)
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true         # Quan inicï la "public subnet", se li assginarà una IP pública existent.
  availability_zone       = var.Azone2 # Escollim "a" per si necessitem utilitzar més "Availiability Zones" en un futur.
  tags = {Name = "principal-public2"}   // DE FET AQUESTA ES LA NETWORK LOAD BALANCER QUE MENCIONEN
}


resource "aws_route_table_association" "main_public_assoc2" { # vinculem la subxarxa publica a la taula d'enrutament
count = "1"
  subnet_id      = aws_subnet.public_subnet_2.id
  route_table_id = aws_route_table.main_public_rt_1[count.index].id
}

# SUBNET PRIVADA 1

resource "aws_subnet" "private1_subnet" {
  vpc_id                  = aws_vpc.main_vpc.id # Assignem el valor de la "id" de la nostra subnet (main_vpc)
  cidr_block              = "10.0.3.0/24"
  //map_public_ip_on_launch = true         # Quan inicï la "public subnet", se li assginarà una IP pública existent.
  map_public_ip_on_launch = false
  availability_zone       = var.Azone # Escollim "a" per si necessitem utilitzar més "Availiability Zones" en un futur.
  tags = {Name = "principal-private1"}
}

resource "aws_route_table" "main_private_rt" { # rt = route table, per a la subxarxa privada1
  vpc_id = aws_vpc.main_vpc.id
  tags =  { Name = "principal_private_rt" }
}

resource "aws_route" "default_route_pr1" {   // default_route, podem sortir on volguem
  count = "1"
  route_table_id         = aws_route_table.main_private_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_nat_gateway.nat1[count.index].id
}

resource "aws_route_table_association" "main_private1_assoc" { 
# vinculem la subxarxa privada1 a la taula d'enrutament
  subnet_id      = aws_subnet.private1_subnet.id
  route_table_id = aws_route_table.main_private_rt.id
}


# SUBNET PRIVADA 2

resource "aws_subnet" "private2_subnet" {
  vpc_id                  = aws_vpc.main_vpc.id # Assignem el valor de la "id" de la nostra subnet (main_vpc)
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = false
  //map_public_ip_on_launch = true         # Quan inicï la "public subnet", se li assginarà una IP pública existent.
  availability_zone       = var.Azone2 # Escollim "a" per si necessitem utilitzar més "Availiability Zones" en un futur.
  tags = {Name = "principal-private2"}
}

resource "aws_route_table" "main_private_rt2" { # rt = route table, per a la subxarxa privada1
  count = "1"
  vpc_id = aws_vpc.main_vpc.id
  route  {
    cidr_block = "0.0.0.0/0"
   //nat_gateway_id             = aws_nat_gateway.nat2[count.index].id
   nat_gateway_id             = aws_nat_gateway.nat2[count.index].id
  }

  tags =  { Name = "principal_private_rt2" }
}

resource "aws_route_table_association" "main_private2_assoc" { 
# vinculem la subxarxa privada1 a la taula d'enrutament
  subnet_id      = aws_subnet.private2_subnet.id
  route_table_id = aws_route_table.main_private_rt.id
}
